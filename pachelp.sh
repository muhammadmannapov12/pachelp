#!/bin/bash

updatesys()
{
sudo pacman -Syu
}

update()
{
read -p Update: update
sudo pacman -Syu $update
}

help()
{
echo "help - shows you all of the commands"
echo "update - update a package"
echo "updatesys - updates you system"
echo "remove - remove a package"
echo "install - install a package"
echo "exit - to exit without doing anything"
}

remove()
{
read -p Remove: remove
sudo pacman -Rs $remove
}

install()
{
read -p Install: install
sudo pacman -S $install
}

echo "type help if you dont know the commands"
read -p Command: cm
$cm
